from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

class AdminConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    label = "eventsd_admin"
    name = "eventsd.admin"
    verbose_name = _("Administration")