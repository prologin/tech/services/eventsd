from django.urls import path
from eventsd.admin import views

app_name = 'eventsd_admin'

urlpatterns = [
    path('', views.AdminHomeView.as_view(), name='home'),
]