from django import forms
from django.utils.translation import gettext_lazy as _
from crispy_forms import layout, helper
from importlib import import_module
from django.conf import settings

def get_signup_form_helper():
    module_name, func_name = settings.EVENTSD_SIGNUP_FORM_HELPER.rsplit('.', 1)
    module = import_module(module_name)
    return getattr(module, func_name)

SIGNUP_FORM_HELPER = get_signup_form_helper()

class EventSignupForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.form_object = kwargs.pop("form_object")  # noqa
        super().__init__(*args, **kwargs)
        first_name = forms.fields.CharField(
            label=_("Prénom"),
            max_length=256,
            label_suffix="",
            required=True,
        )

        last_name = forms.fields.CharField(
            label=_("Nom"),
            max_length=256,
            label_suffix="",
            required=True,
        )

        dob = forms.fields.DateField(
            label=_("Date de naisssance"),
            required=True,
        )

        self.initial_fields = {
            "first_name": first_name,
            "last_name": last_name,
            "dob": dob,
        }
        self.form_object_fields = self.form_object.get_form_fields()
        self.fields = self.initial_fields | self.form_object_fields
        self.helper = SIGNUP_FORM_HELPER(self)