from django.urls import path, include
from . import views

app_name = "eventsd_events"

urlpatterns = [
    path("", views.UpcomingEventListView.as_view(), name='upcoming-events'),
    path("<int:pk>/", views.EventDetailView.as_view(), name='event-detail'),
    path("<int:pk>/signup", views.SignupView.as_view(), name='event-signup'),

    path("attendee/<int:pk>", views.AttendeeDetailView.as_view(), name='attendee-detail'),
    path("attendee/<int:pk>/confirm", views.ConfirmAttendanceView.as_view(), name='attendee-confirm'),
]
