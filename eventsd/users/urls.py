from django.urls import path
from . import views

app_name = "eventsd_users"

urlpatterns = [
    path('<int:pk>/', views.UserProfileDetailView.as_view(), name='profile-detail'),
]
