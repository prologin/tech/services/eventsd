from django.views.generic import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from . import models

class UserProfileDetailView(LoginRequiredMixin, DetailView):
    template_name = 'eventsd/users/profile-detail.html'

    def get_queryset(self):
        if self.request.user.has_perm("eventsd_users.can_view_user"):
            return models.User.objects.all()
        return models.User.objects.filter(id=self.request.user.id)